#include "sdltools.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <SDL.h>
#include <SDL2/SDL_ttf.h>

#define LVL_1_SIZE 1048576//1Mb page size. Every subsequent size is / 2
#define ELEM_PER_PAGE 1024

#define STATUS_INACTIVE 0
#define STATUS_UPDATING 1
#define STATUS_FORCE_REUPDATE 2

sig_atomic_t update_status = STATUS_INACTIVE;

struct memelement *starting_element;
sig_atomic_t curr_zoom = 0;
sig_atomic_t offset = 0;

int is_zoom = 0;

TTF_Font *sans_font;

struct offset_arr {
	struct offset_arr *next;
	long long int val;	
};

struct offset_arr *first;

void register_offset(long long int val) {

	if(first == NULL) {

		first = malloc(sizeof(struct offset_arr));
		first->val = val;
		first->next = NULL;

	} else {

		struct offset_arr *next = first;
		first = malloc(sizeof(struct offset_arr));
		first->val = val;
		first->next = next;

	}

}

long long int get_last_offset() {

	long long int val = first->val;
	struct offset_arr *orig_first = first;
	first = first->next;
	free(orig_first);

	return val;

}

uintptr_t virt_to_phys(uintptr_t vaddr, pid_t target_pid) {

    FILE *pagemap;
    intptr_t paddr = 0;
    uint64_t offset = (vaddr / sysconf(_SC_PAGESIZE)) * sizeof(uint64_t);
    uint64_t e;

    char pagename[64];
    sprintf(pagename, "/proc/%d/pagemap", target_pid);

    if ((pagemap = fopen(pagename, "r"))) {

        if (lseek(fileno(pagemap), offset, SEEK_SET) == offset) {

            if (fread(&e, sizeof(uint64_t), 1, pagemap)) {

                if (e & (1ULL << 63)) { //Can we read pageinfo ?

                    paddr = e & ((1ULL << 54) - 1);
                    paddr = paddr * sysconf(_SC_PAGESIZE);
                    paddr = paddr | (vaddr & (sysconf(_SC_PAGESIZE) - 1));

                }
            }
        }
        fclose(pagemap);
    }

    return paddr;
}

void register_memdiff(uintptr_t relative_addr, uintptr_t absol_addr, pid_t target_pid) {

    //printf("Diff at %ld\n", ptr_addr);
    if(!starting_element) {

        starting_element = malloc(sizeof(struct memelement));
        starting_element->ptr = relative_addr;
        starting_element->phys_ptr = virt_to_phys(absol_addr, target_pid);
        starting_element->next = NULL;
        printf("Diff at offset %d (phys: %p)\n", starting_element->ptr, starting_element->phys_ptr);

    } else {

        struct memelement *last_element = starting_element;

        while(last_element->next)
            last_element = last_element->next;

        struct memelement *new_element = malloc(sizeof(struct memelement));
        new_element->ptr = relative_addr;
        new_element->phys_ptr = virt_to_phys(absol_addr, target_pid);
        new_element->next = NULL;
        last_element->next = new_element;
        printf("Diff at offset %d (phys: %p)\n", new_element->ptr, new_element->phys_ptr);
    }



}

void draw_text(SDL_Renderer *renderer, int x, int y, int width, int height, char *text) {
    SDL_Color text_color = {255, 255, 255};
    int req_height, req_width;
    TTF_SizeText(sans_font, text, &req_width, &req_height);
    SDL_Surface *message = TTF_RenderText_Blended(sans_font, text, text_color);
    SDL_Texture *message_texture = SDL_CreateTextureFromSurface(renderer, message);
    height = (req_height < height) ? req_height : height;
    width = (req_width < width) ? req_width : width;
    SDL_Rect dest_rect = {x, y, width, height};
    SDL_RenderCopy(renderer, message_texture, NULL, &dest_rect);
    SDL_RenderPresent(renderer);
}

void titlebar_print(SDL_Renderer *renderer) {

    char *text = malloc(sizeof(char) * 256);
    sprintf(text, "Current zoom level = %d, block size = %d", curr_zoom, LVL_1_SIZE / (curr_zoom + 1));
    draw_text(renderer, 0, 0, 512, 40, text);
    free(text);

}


void tooglePixel(SDL_Renderer *renderer, int x, int y) {


    SDL_SetRenderDrawColor(renderer, 0, 255, 255, 1);
    SDL_RenderDrawPoint(renderer, x, y);
    SDL_RenderPresent(renderer);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 1);


}

void draw_rect(SDL_Renderer *renderer, int x, int y, int width, int height) {
    SDL_Rect rect = {x, y, width, height};

    SDL_SetRenderDrawColor(renderer, 0, 255, 255, 1);
    SDL_RenderDrawRect(renderer, &rect);
    SDL_SetRenderDrawColor(renderer, 0, 0, 0, 1);

}


void draw_mem_rect(SDL_Renderer *renderer, int x, int y) {

    draw_rect(renderer, x * 16, y * 16 + 40, 16, 16);

}

//Pagesize => 500*512, 256k chunk
void render_page(SDL_Renderer *renderer) {
    if(update_status != STATUS_INACTIVE) return;
    update_status = STATUS_UPDATING;

    SDL_RenderClear(renderer);
    titlebar_print(renderer);

    unsigned long lower_bound = offset;
    unsigned long upper_bound = offset + (LVL_1_SIZE / (curr_zoom + 1));

    struct memelement *forward_element = starting_element;

    int previous_x, previous_y = -1;
    while(forward_element) {
        if(forward_element->ptr >= lower_bound && forward_element->ptr <= upper_bound) {
          int element_page = (forward_element->ptr - lower_bound) / (LVL_1_SIZE / (curr_zoom + 1));
          int element_y = element_page / 31;
          int element_x = element_page % 31;

          if(element_x != previous_x || element_y != previous_y) {
              previous_x = element_x;
              previous_y = element_y;
              draw_mem_rect(renderer, element_x, element_y);
          }
        }
        if(update_status == STATUS_FORCE_REUPDATE) {
              render_page(renderer);
              return;
          }
        forward_element = forward_element->next;
    }

    SDL_RenderPresent(renderer);
    update_status = STATUS_INACTIVE;
}

SDL_Renderer *sdl_window_show() {

    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_NOPARACHUTE) != 0) {
        printf("Unable to open video output device.\n");
        exit(1);
    }
    SDL_Window *main_window = SDL_CreateWindow("memdifftool", 100, 100, 512, 552, SDL_WINDOW_SHOWN);
    if(main_window == NULL) {
        printf("Unable to create window : %s\n", SDL_GetError());
        exit(1);
    }
    SDL_Renderer *main_renderer = SDL_CreateRenderer(main_window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (main_renderer == NULL) {
        printf("Unable to create renderer : %s\n", SDL_GetError());
        exit(1);
    }

    if(TTF_Init() < 0) {
        printf("Unable to open ttf renderer !\n");
    }

    sans_font = TTF_OpenFont("deps/sans.ttf", 24);

    return main_renderer;

}

void *sdl_input_main(void *args) {

    int quit = 0;
    SDL_Event event_pool;

    while(!quit) {
        while(SDL_WaitEvent(&event_pool)) {
            if(event_pool.type == SDL_QUIT) {
                exit(1);
            } else if(event_pool.type == SDL_MOUSEBUTTONDOWN) {
                int x, y;
                SDL_GetMouseState(&x, &y);
                y -= 40;
                int coord_x = x / 16;
                int coord_y = y / 16;

                if(event_pool.button.button == SDL_BUTTON_LEFT) {

                    int pagenumber = coord_y * 32 + coord_x;
		    if(pagenumber < 0) pagenumber = 0;

                    offset += pagenumber * (LVL_1_SIZE / (curr_zoom + 1));
		    register_offset(offset);
                    curr_zoom += 1 + is_zoom * 9;

                } else if(event_pool.button.button == SDL_BUTTON_RIGHT && curr_zoom != 0) {
                    curr_zoom -= 1 + is_zoom * 9;
                    curr_zoom = (curr_zoom < 0) ? 0 : curr_zoom;
                    offset = get_last_offset();
                }
                if(update_status != STATUS_INACTIVE) {
                    update_status = STATUS_FORCE_REUPDATE;
                } else {
                    render_page(args);
                }
            } else if(event_pool.type == SDL_KEYDOWN) {
                if(event_pool.key.keysym.sym == SDLK_RSHIFT || event_pool.key.keysym.sym == SDLK_LSHIFT) {
                    is_zoom = 1;
                }
            } else if(event_pool.type == SDL_KEYUP) {
                if(event_pool.key.keysym.sym == SDLK_RSHIFT || event_pool.key.keysym.sym == SDLK_LSHIFT) {
                    is_zoom = 0;
                }
            }
        }
    }

}

