#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#include <assert.h>
#include <dirent.h>
#include <pthread.h>
#include <SDL.h>
#include "sdltools.h"


char FIFO_PATH[] = "/tmp/hammer.fifo";

struct target {

    long pid;
    long start_ptr;
    size_t buffer_size;

};

struct target *connectToTarget() {

    struct target *processTarget = malloc(sizeof(struct target));
    int fd = open(FIFO_PATH, O_RDONLY);
    read(fd, &processTarget->pid, sizeof(long));
    read(fd, &processTarget->start_ptr, sizeof(long));
    read(fd, &processTarget->buffer_size, sizeof(size_t));
    close(fd);
    return processTarget;
}

int get_paging_fd(pid_t pid, long addr) {

    DIR *dirlist;
    struct dirent *entry;

    char filename[64];
    sprintf(filename, "/proc/%d/map_files/", pid);

    dirlist = opendir(filename);
    if(dirlist) {
        while((entry = readdir(dirlist)) != NULL) {
            uintptr_t *addr1 = malloc(sizeof(uintptr_t));
            uintptr_t *addr2 = malloc(sizeof(uintptr_t));
            if(strcmp(".", entry->d_name) && strcmp("..", entry->d_name) ) {
                sscanf(entry->d_name, "%lx-%lx", addr1, addr2);
                if(addr >= *addr1 && addr <= *addr2) {
                    char pagename[512];
                    sprintf(pagename, "/proc/%d/map_files/%lx-%lx", pid, *addr1, *addr2);
                    printf("Opening map at %s\n", pagename);
                    return open(pagename, O_RDONLY);
                }
            }
            free(addr1);
            free(addr2);
        }
    }

}

int is_swap_enabled() {

    FILE *swap_file = fopen("/proc/swaps", "r");
    if(swap_file > 0) {

        lseek(fileno(swap_file), 37, SEEK_CUR);
        if(fgetc(swap_file) == -1) {
            return 0;
        }

    } else {
        perror("Unable to open /proc/swaps \n");
    }

    return 1;

}

void *read_pagemap(int file_descriptor, size_t size, int offset) {

    void *return_ptr = malloc(size);
    if(pread(file_descriptor, return_ptr, size, offset) != size) {
        perror("Unable to read mapping.\n");
        exit(1);
    }

    return return_ptr;

}

int memcmp_local(const void* s1, const void* s2,size_t n, struct target *target_info)
{
    size_t orig_size = n;
    const unsigned char *p1 = s1, *p2 = s2;
    while(n--) {
        if( *p1 != *p2 ) {
            register_memdiff(orig_size - n - 1, target_info->start_ptr + orig_size - n - 1, target_info->pid);
        }
        p1++,p2++;
    }
    return 0;
}

int main() {

    if(getuid() != 0) {
        printf("Warning: The program is not running as root. It will probably fail\n");
    }

    if(is_swap_enabled()) {
        printf("Swap is enabled ! This can cause problems with paging-out !\n");
    }


    printf("Waiting for the profiler launch...\n");
    struct target *processTarget = connectToTarget();
    printf("Connected to PID %ld. Memory target is 0x%lx, buffer size is %ld\n", processTarget->pid, processTarget->start_ptr, processTarget->buffer_size);

    int main_fd = get_paging_fd(processTarget->pid, processTarget->start_ptr);
    char *initialPointer = read_pagemap (main_fd, processTarget->buffer_size, 0);
    SDL_Renderer *main_renderer = sdl_window_show();

    pthread_t input_thread;
    pthread_create(&input_thread, NULL, sdl_input_main, main_renderer);

    for(;;) {

        sleep(1);

        char *newPointer = read_pagemap (main_fd, processTarget->buffer_size, 0);
        memcmp_local(initialPointer, newPointer, processTarget->buffer_size, processTarget);

        render_page(main_renderer);

        free(initialPointer);
        initialPointer = newPointer;

    }
    close(main_fd);

}
