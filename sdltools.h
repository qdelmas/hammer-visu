#include <stdlib.h>
#include <unistd.h>
#include <SDL.h>

struct sdlconfig {
    int win_height;
    int win_width;
};

struct memelement {

    uintptr_t ptr;
    uintptr_t phys_ptr;
    unsigned long abs_ptr;
    struct memelement *next;

};

SDL_Renderer *sdl_window_show();

void register_memdiff(uintptr_t relative_addr, uintptr_t absol_addr, pid_t target_pid);
void render_page (SDL_Renderer *renderer);

void titlebar_print(SDL_Renderer *renderer);
void *sdl_input_main(void *args);
