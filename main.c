#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <dirent.h>
#include <unistd.h>

char FIFO_PATH[] = "/tmp/hammer.fifo";

int main(int argc, char **argv) {

    size_t length = 1048576 * sizeof(int);
    char *t = mmap(NULL, length, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, -1, 0);
    t[1] = 18;

    if(access(FIFO_PATH, F_OK) != -1) {
        if(unlink(FIFO_PATH) < 0) {

            perror("FIFO exists, but could not be deleted. Try running as root!");

        }

    }

    int comfifo = mkfifo(FIFO_PATH, 0666);
    if(comfifo < 0) {
        perror("Unable to create FIFO, can user write to /tmp ?\n");
        exit(1);
    }

    printf("Waiting for process to connect...\n");
    int fd = open(FIFO_PATH, O_WRONLY);
    if(fd < 0) {
        perror("Unable tobufsize write to FIFO, can the user write to /tmp ?");
    }

    long pid = getpid();
    long bufsize = 1048576 * sizeof(int);

    write(fd, &pid, sizeof(long));
    write(fd, &t, sizeof(long));
    write(fd, &bufsize, sizeof(size_t));

    close(fd);


    printf("PID=%d, PTR=%p\n", getpid(), t);
    printf("%d\n", t[1]);
    sleep(4);
    t[1] = 'd';
    t[524288] = 'f';
    printf("Changed !\n");
    sleep(288928340);

}
